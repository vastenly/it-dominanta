package task3;

public class Fib {
	
	private static int fib(int n) {
		if (n>92)
			throw new IllegalStateException("n>92 will return number bigger than int_max. Should be used BigInteger instead");
		if (n<2)
			return 1;
        return fib(n-2) + fib(n-1);
    }
	
	private static int fibImproved(int n) {
		if (n>92)
			throw new IllegalStateException("n>92 will return number bigger than int_max. Should be used BigInteger instead");
		return n<=2 ? 1 : fib(n-2) + fib(n-1);
    }
	
	public static  void main(String[] args) {	  		  			
		int n = 5;
		
		long time = System.nanoTime();
		System.out.println("[fib method] Time taken = " + (System.nanoTime() - time)
				+  "\n & return result: " + fib(n));	
	
		time = System.nanoTime();
		System.out.println("[fibImproved method] Time taken = " + (System.nanoTime() - time)
				+  "\n & return result: " + fibImproved(n));	
	}
}
