package task2;

public class Palindrome {
	
	private static boolean isPalindrome(String str) {
		if (str == null)
            return false;
		str = str.replaceAll("\\s+","").toLowerCase();
		StringBuilder builder = new StringBuilder(str);
        builder.reverse();
        return builder.toString().equals(str);
    }
		
	public static  void main(String[] args) {	  		  			
		System.out.println(isPalindrome("� ���� ����� �� ���� �����"));	  		  			
	}	  
}
