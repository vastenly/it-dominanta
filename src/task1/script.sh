#!/bin/bash


me=$0
old=@Author
new="@Author Candidate Surname"
dir='./'
cur_dir=$(pwd)

echo Starting $cur_dir $me script to list or to edit files from $dir directory

for i in $(find $dir -type f) 
do
	if [[ $i == *.java || $i == *.cpp ]] 
	then 
		if ! grep -q $old $i
		then
			echo $i
		else
			sed -i '' 's/'$old'.*/'"$new"'/g' $i
		fi
	fi
done

exit